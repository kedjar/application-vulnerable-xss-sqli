﻿using ApplicationVulnérableXSSSQLi.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ApplicationVulnérableXSSSQLi.Controllers
{
    public class CustomerController : Controller
    {
        private readonly BikeStoresDataContext _dtContext;

        public CustomerController(BikeStoresDataContext dtContext)
        {
            this._dtContext = dtContext;
        }

        [HttpGet]
        public ActionResult Connexion()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Connexion(string email)
        {
            bool verif = _dtContext.VerifierClient(email);
            if (verif)
            {
                this.Response.Cookies.Append("email", email, new CookieOptions { HttpOnly = true });

                return View("Verifie");
            }
            return View();
        }

        // GET: CustomerController/Create
        public ActionResult Verifie()
        {
            // TODO: vulnérabilité LFI
            return View();
        }

        // GET: CustomerController/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: CustomerController/Create
        [HttpPost]
        public ActionResult Create(IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: CustomerController/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: CustomerController/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: CustomerController/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: CustomerController/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}
