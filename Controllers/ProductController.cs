﻿using ApplicationVulnérableXSSSQLi.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Data.SqlClient;

namespace ApplicationVulnérableXSSSQLi.Controllers
{
    public class ProductController : Controller
    {

        private readonly BikeStoresDataContext _dtContext;

        public ProductController(BikeStoresDataContext dtContext)
        {
            this._dtContext = dtContext;
        }

        // GET: ProductController
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Liste(string searchString, int id, string productName)
        {
            List<Product> listeProduits = this._dtContext.Select();


            if (!String.IsNullOrEmpty(searchString))
            {
                searchString = searchString.ToLower();
                listeProduits = this._dtContext.Research(searchString);
                ViewBag.searchString = searchString;

            }


            if (id != 0)
            {
                ViewBag.identifiant = id;
            }            
            
            if (!String.IsNullOrEmpty(productName))
            {
                ViewBag.productName = productName;
            }
                
            return View(listeProduits);
        }


        // GET: ProductController/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: ProductController/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: ProductController/Create
        [HttpPost]
        public ActionResult Create(Product produit)
        {
            this._dtContext.Inserer(produit);
            return RedirectToAction(nameof(Liste));

        }


        // GET: ProductController/Edit/5
        public ActionResult Edit(int id)
        {
            Product produit = this._dtContext.Select(id);
            return View(produit);
        }

        // POST: ProductController/Edit/5
        [HttpPost]
        public ActionResult Edit(Product produit)
        {
            this._dtContext.Modifier(produit);
            return RedirectToAction(nameof(Liste), new { productName = produit.ProductName });
        }

        // GET: ProductController/Delete/5

        [HttpGet]
        public IActionResult Delete(int id)
        {
            Product produit = this._dtContext.Select(id);
            return View(produit);
        }

        // POST: ProductController/Delete/5
        [HttpPost]
        public IActionResult ConfirmDelete(int id)
        {
            this._dtContext.Supprimer(id);
            return RedirectToAction(nameof(Liste), new { id = id });
        }

        //TODO: Garder le dernier serachString (ou une autre valeur) dans un cookie (HttpOnly à false) et demander aux étudiants de le corriger
    }
}
