## Instructions
### Partie 1 : XSS et SQLi - Forked

- Clonez le dépôt `git clone https://gitlab.com/dalicia.bouallouche/application-vulnerable-xss-sqli.git`
- Installez la base de données BikeStores (https://www.sqlservertutorial.net/load-sample-database/)
- Lancez l'application, testez-la et familiarisez-vous avec (votre meilleur amis est le débogueur!).
- Il y a (au moins) 5 faillez XSS et 3 failles d'injection SQL. Trouvez-les et corrigez-les!
- Dans la page Connexion, un cookie gardant l'email de l'usager a été ajouté. 
	- Faites en sorte que les cookies ne puissent pas être accessibles via du javascript (indice: HttpOnly).
	- Une injection SQL va vous permettre de vous connecter sans connaître d'email. Corrigez-la.


**Indices pour les injections SQL dans l'ajout et la modification d'un produit:**

```text',1,1,2022,9999);DELETE FROM production.products; --```

```text';DELETE FROM production.products --```

### Partie 2 : Path Traversal (LFI) et Unrestricted File Upload
Mettez vous dans la peau d'un développeur qui doit faire la revue de code de son collègue. 

Votre collègue de travail a ajouté 2 fonctionnalités de téléchargement et de téléversement de fichiers à l'application. Le code qu'il a produit est mal écrit et est plein de failles de sécurité. Vous devez faire une revue de code des fonctionnalités ajoutées et découvrir les failles de sécurité avant de les corriger (servez-vous du débogueur!).

**Documentation:**

https://blog.mindedsecurity.com/2018/10/how-to-prevent-path-traversal-in-net.html (Attention particulière aux sections : Incorrect Path Validation, Incorrect Path Sanitization et How to fix.)

**Indices:**

 - Preuve que le _Path traversal_ est possible : Téléchargez un fichier, pas exemple, le fichier de configuration appsettings.json. Essayez également de télécharger un fichier en dehors du répertoire de l'application (exemple: dans le C:\chemin\vers\fichier).
 - Les failles de type _Path traversal_ se trouvent à 2 endroits différents: l'URL et ... (à vous de trouver).
 - Preuve que la faille _Unrestricted File Upload_ est possible: téléversez un fichier (autre qu'une image) et vérifiez que celle-ci se retrouve dans le code source.

 **Checklist (non-exhaustive car celà dépend de l'application et de son environnement) :**

 - Utiliser une _White list_: créer une liste stricte de valeurs acceptées et rejetez toute entrée qui n'est pas strictement conforme à cette liste. Cette solution est valable lorsqu'une telle liste est connue.
 - Ne pas compter sur l'exlusion d'entrées malicieuses, vous pouvez manquer quelques unes!
 - Autoriser le '.' pour les noms de fichiers mais ne pas autoriser les '/' ou '\' (chemins absolus) '../' ou '..\\' (chemins relatifs). 
 - Le filtrage de '../' ou '..\\' peut produire un chemin qui donne le même accès dangereux (https://cwe.mitre.org/data/definitions/182.html). Exemple: si les séquences '../' sont supprimées d'un chemin, la séquence suivante ".../...//" peut contourner une telle protection.
 - Utiliser une liste d'extensions de fichiers permises.
 - Ne pas compter sur un filtrage de caractères potentiellement dangereux. Elle pourrait ne pas être exshaustive! Ce qui donnerait un faux sentiment de sécurité. (https://cwe.mitre.org/data/definitions/184.html)
 - Avant la validation, vous devez décoder et canoniser (définition de la canonisation [ici](https://blog.mindedsecurity.com/2018/10/how-to-prevent-path-traversal-in-net.html) dans la section **How To Fix**) l'entrée avant.

**Note:** Une combinaison de plusieurs solutions peut-être nécessaire.

Sources:

https://blog.mindedsecurity.com/2018/10/how-to-prevent-path-traversal-in-net.html

https://cwe.mitre.org/data/definitions/22.html

https://owasp.org/www-community/attacks/Path_Traversal

https://owasp.org/www-community/vulnerabilities/Unrestricted_File_Upload
